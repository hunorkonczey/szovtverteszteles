package ro.sapientia2015.story;

import org.springframework.test.util.ReflectionTestUtils;

import ro.sapientia2015.story.dto.SprintDTO;
import ro.sapientia2015.story.dto.StoryDTO;
import ro.sapientia2015.story.model.Sprint;
import ro.sapientia2015.story.model.Story;

public class SprintTestUtil {

    public static final Long ID = 1L;
    public static final String DESCRIPTION = "description";
    public static final String DESCRIPTION_UPDATED = "updatedDescription";
    public static final String TITLE = "title";
    public static final String TITLE_UPDATED = "updatedTitle";
    public static final String USER = "user";
    public static final String USER_UPDATED = "updatedUser";

    private static final String CHARACTER = "a";

    public static SprintDTO createFormObject(Long id, String description, String title, String user) {
        SprintDTO dto = new SprintDTO();

        dto.setId(id);
        dto.setDescription(description);
        dto.setTitle(title);
        dto.setUser(user);

        return dto;
    }

    public static Sprint createModel(Long id, String description, String title, String user) {
        Sprint model = Sprint.getBuilder(title)
                .description(description)
                .user(user)
                .build();

        ReflectionTestUtils.setField(model, "id", id);

        return model;
    }

    public static String createRedirectViewPath(String path) {
        StringBuilder redirectViewPath = new StringBuilder();
        redirectViewPath.append("redirect:");
        redirectViewPath.append(path);
        return redirectViewPath.toString();
    }

    public static String createStringWithLength(int length) {
        StringBuilder builder = new StringBuilder();

        for (int index = 0; index < length; index++) {
            builder.append(CHARACTER);
        }

        return builder.toString();
    }
}
